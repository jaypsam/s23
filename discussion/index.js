// alert("Hello Batch 197!")

/*
	Objects
		 an object is a data type that is used to represent real world objects. It is also a collection of related data and/or functionalities.

	Creating objects using object literal:
		Syntax:
			let objectName = {
					keyA: valueA,
					keyB: valueB
			}

*/

let student = {
	firstName: "Rupert",
	lastName: "Ramos",
	age: 30,
	StudentId: "2022-009752",
	email: ["rupert.ramos@mail.com", "RBR1209@gmail.com"],
	address: {
		street: "125 Ilang-Ilang St.",
		city: "Quezon City",
		country: "Philippines"
	}
}

console.log("Result from creating an object:")
console.log(student)
console.log(typeof student)


// Creating Objects using Constructor Function
/*
	Create a resuable function to create several objects that have the same data structure. This is useful for creating multiple instances/copies of an object.

	Syntax:
		function ObjectName(valueA, valueB) {
			this.keyA = valueA
			this.keyB = valueB
		}

		let variable = new function ObjectName(valueA, valueB)

		console.log(variable)

			-"this" is a keyword that is used for invoking; in refers to the global object
			- don't forget to add "new" keyword when creating the variables.
*/
// We use Pascal Casing for the ObjectName when creating objects using constructor function.
function Laptop(name, manufactureDate) {
	this.name = name 
	this.manufactureDate = manufactureDate
}

let laptop = new Laptop("Lenovo", 2008);
console.log("Result of creating objects using object constructor:")
console.log(laptop)

let myLaptop = new Laptop("MacBook Air", [2020, 2021])
console.log("Result of creating objects using object constructor:")
console.log(myLaptop)

let oldLaptop = Laptop("Portal R2E CCMC", 1980);
console.log("Result of creating objects using object constructor:")
console.log(oldLaptop)


// Creating empty object as placeholder
let computer = {}
let myComputer = new Object()
console.log(computer)
console.log(myComputer)

myComputer = {
	name: "Asus",
	manufactureDate: 2012
}

console.log(myComputer)

/*MINI ACTIVITY
	- Create an object constructor function to produce 2 objects with 3 key-value pairs
	- Lof the 2 new objects in the console and send SS in our GC.

*/

//solution
function Laptop1(name, manufactureDate, status) {
	this.name = name 
	this.manufactureDate = manufactureDate
	this.status = status
}

let laptop1 = new Laptop1("Lenovo", 2008, "Active");
console.log("Result of creating objects using object constructor:")
console.log(laptop1)

let laptop2 = new Laptop1("Asus", 2008, "Active");
console.log("Result of creating objects using object constructor:")
console.log(laptop2)

//Accessing object properties

//Using the dot notation
//Syntax: objectNmae.prpertyName
console.log(" ")
console.log("Results from don notation " + myLaptop.name)

//Using the bracket notation
//Syntax: objectName["name"]
console.log("Results from don notation " + myLaptop["name"])

//Accessing array objects

let array = [laptop, myLaptop];
//let array = [{name: "Lenovo", manufactureDate: 2008},{name: MacBook Air, manufactureDate: [2019,2020]}];

//Dot notation
console.log(array[0].name);

//Square bracket notation
console.log(array[0]["name"]);

//Initializing / adding / deleting / reassining object properties

let car = {};
console.log(car);

//adding object properties
car.name = "Honda Civic"
console.log("Results from adding property using dot notation:");
console.log(car);

car["manufacture date:"] = 2019
console.log(car);

//deleting object properties
//car ["manufacture date"] = " ";
delete car["manufacture date:"]
console.log("Result from deleting object properties:")
console.log(car);

//Reassingim=ng object properties
car.name = "Tesla";
console.log("Results from reaasinging object properties:")
console.log(car);

console.log(" ");

/* Object Method
	 a method where a function serves as a value in a property. They are also functions and one of the key differences they have is that methods are function related to a specific object property.


*/

let person = {
	name: "John",
	talk: function() {
		console.log("Hello! My name is " + this.name);
	}
}

console.log(person)
console.log("Result from object methods:")
person.talk()

person.walk = function() {
	console.log(this.name + " walked 25 steps forward.")
}

person.walk()


let friend = {
	firstName: "John",
	lastName: "Doe",
	address: {
		city: "Austin, Texas",
		country: "US"
	},
	emails: ["johnD@mail.com", "joe12@yahoo.com"],
	introduce: function() {
		console.log("Hello! My name is " + this.firstName + " " + this.lastName)
	}
}

friend.introduce()

// Real World Application

/*
	Scenario:
		1. We would like to creat a game that would have sevral pokemon to interact with each other.
		2. every pokemon would have the same sets of stats, properties and functions.

*/

// Using Object Literals
/*let myPokemon = {
		name: "Pikachu",
		level: 3,
		health: 100,
		attack: 50,
		tackle: function() {
			console.log("This pokemon takcled tartetPokemon")
			console.log("tartetPokemon's health is now reduced to targetPokemonHealth")
		},
		faint: function() {
			console.log("Pokemon fainted")
		}

}

console.log(myPokemon)


// Using Object Constructor
function Pokemon(name, level) {

	// Properties
	this.name = name
	this.level = level
	this.health = 3 *level
	this.attack = 2 * level

	// Methods
	this.tackle = function(target) {
		console.log(this.name + " tackled "  + target.name)
		console.log(target.name + "'s health is now reduced " + (target.health - this.attack)) 
	},
	this.faint = function() {
		console.log(this.name + " fainted")
	}
}

let charizard = new Pokemon("Charizard", 12)
let squirtle = new Pokemon("Squirtle", 6)

console.log(charizard)
console.log(squirtle)

charizard.tackle(squirtle)
*/
//MAIN ACTIVITY:
/*
WDC028v1.5b-23 | JavaScript - Objects
Graded Activity:

1.) Create a new set of pokemon for pokemon battle. (Same as our discussion)
	- Solve the health of the pokemon that when tackle is invoked, current value of target's health should decrease continuously as many times the tackle is invoked.
	(target.health - this.attack)

2.) If health is less than or equal to 5, invoke faint function

*/
//Code Here:


let player = {
	firstName: "Joshua",
	age: 30,
	friends: ["Emman","Iwi"],
	pokemon: ["Pikachu", "Charizard","Squirtle","Bulbasour"],

	talkOk: function(){
		console.log("Hello! Player 1: " + this.firstName);
		console.log("Your Age is: " + this.age);
		console.log("Your Friends: " + this.friends);
		console.log("Your Collection: " + this.pokemon);
	}
}

console.log(player)
console.log("Results of Players")
player.talkOk()


// Using Object Literal


// Using Object Constructor
function Pokemon(name, level){
	this.name = name
	this.level = level
	this.health = 3 * level
	this.attack = 2 * level

	this.tackle = function(target){
	console.log(" " + this.name + " tackled" + target.name)

		this.faint = function(){
		console.log(" " + target.name + " fainted")
		}

		target.health = (target.health - this.attack);
		if((target.health - this.attack) <= 5){
			this.faint();
		}else{
			console.log(" " + target.name + " health is now reduced to =" + (target.health - this.attack))
		}	
	}
}

let mimiqyu = new Pokemon("Mimiqyu",12)
let girafarig = new Pokemon("Girafarig",19)
let latias = new Pokemon("Latias",19)

console.log(mimiqyu);
console.log(girafarig);
console.log(" ");
mimiqyu.tackle(girafarig);
console.log(" ");
latias.tackle(mimiqyu);














